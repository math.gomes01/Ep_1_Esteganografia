Programa Decodificador_Esteganografia.
Por Lucas Soares Souza

Para compilar o projeto:

1 - Ir ao console do ubuntu (a versão utilizada para teste foi: 15.04)
2 - Ir ao diretorio .../Ep_1_Esteganografia/
3 - Executar o comando make.

Para Executar o projeto:

1 - Ir ao console do ubuntu (a versão utilizada para teste foi: 15.04)
2 - Ir ao diretorio .../Ep_1_Esteganografia/(apos compilar o programa)
3 - Executar o comando make run.

Para Utilizar o projeto:

Escolher uma das opções (1, 2 ou 3) de acordo com a vontade do usuário
	
	*Caso 1 - Se decodifica uma imagem '.pgm'.
	O programa pede o diretório e o nome do arquivo numa mesma string
	Exemplo: /home/usuario/imagem1.pgm.
	Se a imagem não existir ou não for possível abri-la o programa será encerrado.
	Se a imagem não for .pgm o programa também será encerrado.
	Após se escolher a imagem correta o programa altomaticamente ira buscar a posição de inicio da mensagem.
	
	(Observação: Utilizando-se do artefato de que as imagens utilizadas para o projeto tinham um padrão em que na terceira
	linha do arquivo existia um comentário que após a "# " indicava a posição de inicio da mensagem. Se este
	comentário não existir o programa não será executado da forma correta)
	
	Então o programa mostrará a mensagem e as considerações finais.
	Após isso ele será encerrado.

	*Caso 2 - Se decodifica uma imagem '.ppm'.
	O programa solicita o diretório e o nome do arquivo numa mesma string.
	Exemplo: /home/usuario/mensagem1.pgm.
	Se a imagem não existir ou não for possível abri-la o programa será encerrado.
	Se a imagem não for .ppm o programa também será encerrado.
	Caso não haja nenhum erro até aqui. O programa solicitará o local para gravação do novo arquivo.
	Se não for possivel criar o novo arquivo o programa será fechado altomaticamente.
	Apos estas etapas o programa solicitará que o usuário escolha o filtro desejado.
	
	Para mensagem1.ppm se utiliza o filtro 1 - Vermelho.
	Para mensagem2.ppm se utiliza o filtro 2 - Verde.
	Para mensagem3.pmm se utiliza o filtro 3 - Azul.

	O programa então irá enviar ao arquivo aberto a matrix atualizada com o filtro RGB.
	Após isso ele indicará que o arquivo foi criado com sucesso e mostrará as considerações finais.
	Então ele será encerrado.


	*Caso 3 - Encerra-se o programa.


