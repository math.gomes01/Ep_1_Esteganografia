#ifndef CLASSEIMAGEM_H
#define CLASSEIMAGEM_H

#include <iostream>
#include <string>

class Imagem{
	
	private:
		
		//ATRIBUTO
		int PosicaoInicial;
		int Altura;
		int Largura;
		int CorMaxima;
		int Dimensao;
	public:
		//CONSTRUTORES
		Imagem(int PosicaoInicial, int Altura, int Largura, int CorMaxima);
		Imagem(int Altura, int Largura, int CorMaxima); 


		//ACESSORES

		void setPosicaoInicial(int PosicaoInicial);
		int getPosicaoInicial();
		void setAltura(int Altura);
		int getAltura();
		void setLargura(int Largura);
		int getLargura();
		void setCorMaxima(int CorMaxima);
		int getCorMaxima();
		int calculaDimensao();
		void setDimensao(int Dimensao);

};



#endif
